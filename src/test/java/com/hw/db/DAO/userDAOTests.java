package com.hw.db.DAO;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.models.User;

class userDAOTests {
    JdbcTemplate mockJdbc;
    UserDAO userDAO;
    String name;
    String email;
    String about;
    String fullname;

    @BeforeEach
    void setup() {
        mockJdbc = mock(JdbcTemplate.class);
        userDAO = new UserDAO(mockJdbc);
        name = "name";
        email = "email";
        about = "about";
        fullname = "fullname";
    }

    @Test
    @DisplayName("nothing change")
    void nothing() {
        UserDAO.Change(new User(name, null, null, null));
        verify(mockJdbc, never()).update(
                Mockito.any(String.class), Mockito.any(Object[].class));
    }

    @Test
    @DisplayName("email change")
    void email() {
        UserDAO.Change(new User(name, email, null, null));
        verify(mockJdbc).update(
                Mockito.eq(
                        "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(email),
                Mockito.eq(name));
    }

    @Test
    @DisplayName("fullname change")
    void fullname() {
        UserDAO.Change(new User(name, null, email, null));
        verify(mockJdbc).update(
                Mockito.eq(
                        "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(email),
                Mockito.eq(name));
    }

    @Test
    @DisplayName("about change")
    void about() {
        UserDAO.Change(new User(name, null, null, email));
        verify(mockJdbc).update(
                Mockito.eq(
                        "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(email),
                Mockito.eq(name));
    }

    @Test
    @DisplayName("email and fullname change")
    void emailAndFullname() {
        UserDAO.Change(new User(name, email, fullname, null));
        verify(mockJdbc).update(
                Mockito.eq(
                        "UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(email),
                Mockito.eq(fullname),
                Mockito.eq(name));
    }

    @Test
    @DisplayName("email and about change")
    void emailAndAbout() {
        UserDAO.Change(new User(name, email, null, fullname));
        verify(mockJdbc).update(
                Mockito.eq(
                        "UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(email),
                Mockito.eq(fullname),
                Mockito.eq(name));
    }

    @Test
    @DisplayName("fullname and about change")
    void fullnameAndAbout() {
        UserDAO.Change(new User(name, null, email, fullname));
        verify(mockJdbc).update(
                Mockito.eq(
                        "UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(email),
                Mockito.eq(fullname),
                Mockito.eq(name));
    }

    @Test
    @DisplayName("everything change")
    void everything() {
        UserDAO.Change(new User(name, email, fullname, about));
        verify(mockJdbc).update(
                Mockito.eq(
                        "UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq(email),
                Mockito.eq(fullname),
                Mockito.eq(about),
                Mockito.eq(name));
    }

}
