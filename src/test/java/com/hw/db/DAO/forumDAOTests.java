package com.hw.db.DAO;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.UserDAO.UserMapper;

class forumDAOTests {
    String slug;
    String since;
    int limit;
    JdbcTemplate jdbcMock;
    ForumDAO forum;
    UserMapper userMapper;

    @BeforeEach
    void setup() {
        slug = "slug";
        since = "since";
        limit = 1;
        jdbcMock = mock(JdbcTemplate.class);
        forum = new ForumDAO(jdbcMock);
        userMapper = new UserDAO.UserMapper();
    }

    @Test
    @DisplayName("+slug; +limit; +since;")
    void withSinceDescLimitTest() {
        ForumDAO.UserList(slug, limit, since, true);
        verify(jdbcMock).query(Mockito.eq(
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("+slug; -limit; +since;")
    void withoutSinceDescLimitTest() {
        ForumDAO.UserList(slug, null, since, false);
        verify(jdbcMock).query(
                Mockito.eq(
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("+slug; -limit; -since;")
    void withSinceWithoutDescLimitTest() {
        ForumDAO.UserList(slug, null, null, false);
        verify(jdbcMock).query(
                Mockito.eq(
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

}