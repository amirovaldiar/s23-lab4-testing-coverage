package com.hw.db.DAO;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.sql.Timestamp;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.models.Post;

class postDAOTests {

    String oldString;
    Timestamp oldTimestamp;
    String newString;
    Timestamp newTimestamp;
    Post old;
    JdbcTemplate mockJdbc;
    PostDAO postDAO;

    @BeforeEach
    void setup() {
        oldTimestamp = new Timestamp(0);
        oldString = "old";
        newTimestamp = new Timestamp(1);
        newString = "new";
        old = new Post();
        old.setMessage(oldString);
        old.setAuthor(oldString);
        old.setCreated(oldTimestamp);
        mockJdbc = mock(JdbcTemplate.class);
        postDAO = new PostDAO(mockJdbc);
    }

    @Test
    @DisplayName("nothing change")
    void nothing() {
        Mockito.when(
                mockJdbc.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
                .thenReturn(old);

        Post post = new Post();
        post.setAuthor(oldString);
        post.setMessage(oldString);
        post.setCreated(oldTimestamp);

        PostDAO.setPost(0, post);
        verify(mockJdbc, never()).update(Mockito.any(String.class), Mockito.any(Object[].class));
    }

    @Test
    @DisplayName("author change")
    void author() {
        Mockito.when(
                mockJdbc.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
                .thenReturn(old);

        Post post = new Post();
        post.setAuthor(newString);
        post.setMessage(oldString);
        post.setCreated(oldTimestamp);

        PostDAO.setPost(0, post);
        verify(mockJdbc).update(
                Mockito.eq(
                        "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Optional.ofNullable(Mockito.any()));
    }

    @Test
    @DisplayName("message change")
    void messageTest() {
        Mockito.when(
                mockJdbc.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
                .thenReturn(old);

        Post post = new Post();
        post.setAuthor(oldString);
        post.setMessage(newString);
        post.setCreated(oldTimestamp);

        PostDAO.setPost(0, post);
        verify(mockJdbc).update(
                Mockito.eq(
                        "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Optional.ofNullable(Mockito.any()));

    }

    @Test
    @DisplayName("timestamp change")
    void timestamp() {
        Mockito.when(
                mockJdbc.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
                .thenReturn(old);

        Post post = new Post();
        post.setAuthor(oldString);
        post.setMessage(oldString);
        post.setCreated(newTimestamp);

        PostDAO.setPost(0, post);
        verify(mockJdbc).update(
                Mockito.eq(
                        "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Optional.ofNullable(Mockito.any()));

    }

    @Test
    @DisplayName("author and message change")
    void authorAndMessage() {
        Mockito.when(
                mockJdbc.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
                .thenReturn(old);

        Post post = new Post();
        post.setAuthor(newString);
        post.setMessage(newString);
        post.setCreated(oldTimestamp);

        PostDAO.setPost(0, post);
        verify(mockJdbc).update(
                Mockito.eq(
                        "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                Optional.ofNullable(Mockito.any()));

    }

    @Test
    @DisplayName("author and message change")
    void messageAndTimestamp() {
        Mockito.when(
                mockJdbc.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
                .thenReturn(old);

        Post post = new Post();
        post.setAuthor(oldString);
        post.setMessage(newString);
        post.setCreated(newTimestamp);

        PostDAO.setPost(0, post);
        verify(mockJdbc).update(
                Mockito.eq(
                        "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Optional.ofNullable(Mockito.any()));

    }

    @Test
    @DisplayName("everything change")
    void everything() {
        Mockito.when(
                mockJdbc.queryForObject(Mockito.anyString(), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt()))
                .thenReturn(old);

        Post post = new Post();
        post.setAuthor(newString);
        post.setMessage(newString);
        post.setCreated(newTimestamp);

        PostDAO.setPost(0, post);
        verify(mockJdbc).update(
                Mockito.eq(
                        "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Optional.ofNullable(Mockito.any()));

    }
}