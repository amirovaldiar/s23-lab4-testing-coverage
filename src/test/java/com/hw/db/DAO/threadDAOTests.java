package com.hw.db.DAO;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.PostDAO.PostMapper;

class threadDAOTests {
    JdbcTemplate mockJdbc;
    ThreadDAO thread;
    PostMapper postMapper;

    @BeforeEach
    void setup() {
        mockJdbc = mock(JdbcTemplate.class);
        thread = new ThreadDAO(mockJdbc);
        postMapper = new PostMapper();
    }

    @Test
    @DisplayName("desc=true")
    void descTrue() {
        ThreadDAO.treeSort(1, 1, 1, true);
        verify(mockJdbc).query(
                Mockito.eq(
                        "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
                Mockito.any(PostMapper.class), Optional.ofNullable(Mockito.any()));
    }

    @Test
    @DisplayName("desc=false")
    void descFalse() {
        ThreadDAO.treeSort(1, 1, 1, false);
        verify(mockJdbc).query(
                Mockito.eq(
                        "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"),
                Mockito.any(PostMapper.class), Optional.ofNullable(Mockito.any()));
    }

}